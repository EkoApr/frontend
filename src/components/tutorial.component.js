import React, { Component } from "react";
import { connect } from "react-redux";
import { updateTutorial, deleteTutorial } from "../actions/tutorials";
import TutorialDataService from "../services/tutorial.service";

class Tutorial extends Component {
  constructor(props) {
    super(props);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.getTutorial = this.getTutorial.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
    this.updateContent = this.updateContent.bind(this);
    this.removeTutorial = this.removeTutorial.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);

    this.state = {
      currentTutorial: {
        id: null,
        image: "",
        name: "",
        price: 0,
        sellingprice : 0,
        stock : 0,
      },
      message: "",
    };
  }

  componentDidMount() {
    this.getTutorial(this.props.match.params.id);
  }

  onChangeTitle(e) {
    const value = e.target.value;

    this.setState(function (prevState) {
      return {
        currentTutorial: {
          ...prevState.currentTutorial,
          natitleme: value,
        },
      };
    });
  }

  onChangeDescription(e) {
    const description = e.target.value;

    this.setState((prevState) => ({
      currentTutorial: {
        ...prevState.currentTutorial,
        description: description,
      },
    }));
  }

  getTutorial(id) {
    TutorialDataService.get(id)
      .then((response) => {
        this.setState({
          currentTutorial: response.data,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updateStatus(status) {
    var data = {
      id: this.state.currentTutorial.id,
      image: this.state.currentTutorial.image,
      name: this.state.currentTutorial.name,
      price: this.state.currentTutorial.price,
      sellingprice: this.state.currentTutorial.sellingprice,
      stock : this.state.currentTutorial.stock,
    };

    this.props
      .updateTutorial(this.state.currentTutorial.id, data)
      .then((reponse) => {
        console.log(reponse);

        this.setState((prevState) => ({
          currentTutorial: {
            ...prevState.currentTutorial,
            published: status,
          },
        }));

        this.setState({ message: "The status was updated successfully!" });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updateContent() {
    this.props
      .updateTutorial(this.state.currentTutorial.id, this.state.currentTutorial)
      .then((reponse) => {
        console.log(reponse);
        
        this.setState({ message: "The tutorial was updated successfully!" });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  uploadFile(e)
  {
    let files = e.target.files;
    var allFiles = [];
    for (var i = 0; i < files.length; i++) {

      let file = files[i];
      if(file.size <= 100000)
      {
      // Make new FileReader
      let reader = new FileReader();

      // Convert the file to base64 text
      reader.readAsDataURL(file);

      // on reader load somthing...
      reader.onload = () => {

        // Make a fileInfo Object
        let fileInfo = {
          name: file.name,
          type: file.type,
          size: Math.round(file.size / 1000) + ' kB',
          base64: reader.result,
          file: file,
        };

        // Push it to the state
        allFiles.push(fileInfo);

        this.setState(function (prevState) {
          return {
            currentTutorial: {
              ...prevState.currentTutorial,
              image: fileInfo.base64,
            },
          };
        });
      } 
    }else
    {
      files= "";;
      alert("file to large, max. 100Kb");
      return;
    }// reader.onload
  }
  }
  
  onChangeForm(e) {
    var formName = e.target.name;
    const value = e.target.value;

    console.log(formName);

    switch(formName) {

      case "name": 
          this.setState(function (prevState) {
            return {
              currentTutorial: {
                ...prevState.currentTutorial,
                name: value,
              },
            };
          });

        break;

        case "price":  
          this.setState(function (prevState) {
            return {
              currentTutorial: {
                ...prevState.currentTutorial,
                price: value,
              },
            };
          });
       break;

       case "sellingprice":  
       this.setState(function (prevState) {
        return {
          currentTutorial: {
            ...prevState.currentTutorial,
            sellingprice: value,
          },
        };
      });
        break;

        case "stock":  
        this.setState(function (prevState) {
          return {
            currentTutorial: {
              ...prevState.currentTutorial,
              stock: value,
            },
          };
        });
        break;

      default: 
     this.uploadFile(e) 
    }
  }
  removeTutorial() {
    if(window.confirm("Are you sure to Delete?"))
    {
    this.props
      .deleteTutorial(this.state.currentTutorial.id)
      .then(() => {
        this.props.history.push("/tutorials");
      })
      .catch((e) => {
        console.log(e);
      });
    }
  }

  render() {
    const { currentTutorial } = this.state;
    const { tutorials } = this.props;
    
    return (
      <div>
        {currentTutorial ? (
          <div className="edit-form">
            <h4>Tutorial</h4>
            <form>
            <div>
            <div className="form-group">
              <label htmlFor="itemImageInput">Image</label>
              <div className="form-group">
              <input id="image" type="file" className="form-group" accept=".png, .jpg" onChange={this.onChangeForm}/>
              <div>
                <img alt="preview image" width="120" height="120" src={currentTutorial.image?currentTutorial.image : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjx-by4e5r6kDq59HyeRElGTd00oMFr-duASxN8UKnsQ&s"}/>
              </div>
              </div>

              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                readOnly={true}
                value={currentTutorial.name}
                onChange={this.onChangeForm}
                name="name"
              />
            </div>

            <div className="form-group">
              <label htmlFor="price">Price</label>
              <input
                type="number"
                className="form-control"
                id="price"
                value={currentTutorial.price}
                onChange={this.onChangeForm}
                name="price"
              />
            </div>

            <div className="form-group">
              <label htmlFor="sellingprice">Selling Price</label>
              <input
                type="number"
                className="form-control"
                id="sellingprice"
                value={currentTutorial.sellingprice}
                onChange={this.onChangeForm}
                name="sellingprice"
              />
            </div>

            <div className="form-group">
              <label htmlFor="stock">Stock</label>
              <input
                type="number"
                className="form-control"
                id="stock"
                value={currentTutorial.stock}
                onChange={this.onChangeForm}
                name="stock"
              />
            </div>
          </div>
            </form>

            <button
              className="btn btn-danger mr-2"
              onClick={this.removeTutorial}
            >
              Delete
            </button>

            <button
              type="submit"
              className="btn btn-success"
              onClick={this.updateContent}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Tutorial...</p>
          </div>
        )}
      </div>
    );
  }
}

export default connect(null, { updateTutorial, deleteTutorial })(Tutorial);
