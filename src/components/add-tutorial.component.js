import React, { Component } from "react";
import { connect } from "react-redux";
import { createTutorial,retrieveTutorials } from "../actions/tutorials";
import FileBase64 from './react-file-base64.js';

class AddTutorial extends Component {
  constructor(props) {
    super(props);
    this.onChangeForm = this.onChangeForm.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.saveTutorial = this.saveTutorial.bind(this);
    this.newTutorial = this.newTutorial.bind(this);
    this.uploadFile = this.uploadFile.bind(this);
   // this.handleImage = this.handleImage.bind(this)
  
    this.state = {
      files: [],
    };

    this.state = {
      id: null,
      image :"",
      name: "",
      price: "",
      sellingprice: "",
      stock : "",
    };
  }

  uploadFile(e)
  {
    let files = e.target.files;
    var allFiles = [];
    for (var i = 0; i < files.length; i++) {

      let file = files[i];
    if(file.size <= 100000)
    {
      // Make new FileReader
      let reader = new FileReader();

      // Convert the file to base64 text
      reader.readAsDataURL(file);

      // on reader load somthing...
      reader.onloadend = () => {

        // Make a fileInfo Object
        let fileInfo = {
          name: file.name,
          type: file.type,
          size: Math.round(file.size / 1000) + ' kB',
          base64: reader.result,
          file: file,
        };

        this.setState({
          image: fileInfo.base64
        });
        // Push it to the state
        allFiles.push(fileInfo);

        // If all files have been proceed
        // if(allFiles.length == files.length){
        //   // Apply Callback function
        //   if(this.props.multiple) this.props.onDone(allFiles);
        //   else this.props.onDone(allFiles[0]);
        // }
        // reader.onload
      }
    }
    else{
      alert("file to large, max. 100Kb");
      return;}
    }
  }


  onChangeForm(e) {
    var formName = e.target.name;
    console.log(formName);

    switch(formName) {
      // case "image":
      //   // this.setState({
      //   //     image: e.target.value,
      //   //   });
      //   break;
      case "name":  
         this.setState({
            name: e.target.value,
          });
        break;

        case "price":  
        this.setState({
           price: e.target.value,
         });
       break;

       case "sellingprice":  
         this.setState({
            sellingprice: e.target.value,
          });
        break;

        case "stock":  
         this.setState({
            stock: e.target.value,
          });
        break;

      default: 
     this.uploadFile(e) }
    
    // localStorage.setItem("state", this.state.value);

  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value,
    });
  }

  saveTutorial() {
    const { image, name, price, sellingprice, stock } = this.state;

    let nameExist = this.props.tutorials.filter(obj => {
      return obj.name === name})

    if(nameExist.length>0)
      {
        alert("Name " + name + " alrady exist!")
        return;
      }

    this.props
      .createTutorial(image, name, price,sellingprice,stock)
      .then((data) => {
        this.setState({
          id: data.id,
          image: data.image,
          name: data.name,
          price: data.price,
          sellingprice : data.sellingprice,
          stock:data.stock,
          
          submitted : true,
        });
        console.log(data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  newTutorial() {
    this.setState({
      id: null,
      image: "",
      name: "",
      price: 0,
      sellingprice : 0,
      stock : 0,
    });
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>You submitted successfully!</h4>
            <button className="btn btn-success" onClick={this.newTutorial}>
              Add
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="itemImageInput">Image</label>
              <div className="form-group">
              <input id="image" type="file" className="form-group" accept=".png, .jpg" onChange={this.onChangeForm}/>
              <div >
                <img alt="preview image" width="120" height="120" src={this.state.image? this.state.image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjx-by4e5r6kDq59HyeRElGTd00oMFr-duASxN8UKnsQ&s"}/>
              </div>
              </div>

              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                value={this.state.name}
                onChange={this.onChangeForm}
                name="name"
              />
            </div>

            <div className="form-group">
              <label htmlFor="price">Price</label>
              <input
                type="number"
                className="form-control"
                id="price"
                value={this.state.price}
                onChange={this.onChangeForm}
                name="price"
              />
            </div>

            <div className="form-group">
              <label htmlFor="sellingprice">Selling Price</label>
              <input
                type="number"
                className="form-control"
                id="sellingprice"
                value={this.state.sellingprice}
                onChange={this.onChangeForm}
                name="sellingprice"
              />
            </div>

            <div className="form-group">
              <label htmlFor="stock">Stock</label>
              <input
                type="number"
                className="form-control"
                id="stock"
                value={this.state.stock}
                onChange={this.onChangeForm}
                name="stock"
              />
            </div>

            <button onClick={this.saveTutorial} className="btn btn-success">
              Submit
            </button>
          </div>
        )}
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    tutorials: state.tutorials,
  };
};

export default connect(mapStateToProps, { createTutorial,retrieveTutorials })(AddTutorial);
