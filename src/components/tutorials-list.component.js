import React, { Component,useState } from "react";
import { connect } from "react-redux";

import {
  retrieveTutorials,
  findTutorialsByTitle,
  deleteAllTutorials,
  deleteTutorial,
} from "../actions/tutorials";
import { Link } from "react-router-dom";


class TutorialsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.setActiveTutorial = this.setActiveTutorial.bind(this);
    this.findByTitle = this.findByTitle.bind(this);
    this.removeAllTutorials = this.removeAllTutorials.bind(this);
    this.deleteTutorial = this.deleteTutorial.bind(this);

    this.state = {
      currentTutorial: null,
      currentIndex: -1,
      searchTitle: "",
    };
  }

  componentDidMount() {
    this.props.retrieveTutorials();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  refreshData() {
    this.setState({
      currentTutorial: null,
      currentIndex: -1,
    });
  }

  deleteTutorial(e) {
      if(window.confirm("Are you sure to Delete?"))
      {
      // Here you can put your logic or function that needs to be executed as per the use case.
        this.props.deleteTutorial(e)
          .then(() => {
            this.props.history.push("/tutorials");
          })
          .catch((e) => {
            console.log(e);
          });
      }
    }

  setActiveTutorial(tutorial, index) {
    this.setState({
      currentTutorial: tutorial,
      currentIndex: index,
    });
  }

  removeAllTutorials() {
    this.props
      .deleteAllTutorials()
      .then((response) => {
        console.log(response);
        this.refreshData();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  findByTitle() {
    this.refreshData();

    this.props.findTutorialsByTitle(this.state.searchTitle);
  }

  render() {
    const { searchTitle, currentTutorial, currentIndex } = this.state;
    const { tutorials } = this.props;

    return (
      <div className="list row">
        <div className="col-md-8">
        </div>
        <div className="col-md-12">
          <h4>Tutorials List</h4>
        <div className="row">
            <table className="u-full-width col-md-12">
            <thead>
              <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Item Name</th>
                <th>Price</th>
                <th>Selling Price</th>
                <th>Stock</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {tutorials.length > 0 &&
                tutorials.map(({ id,image, name, price,sellingprice,stock }, i) => (
                    <tr key={i}>
                    <td>{id}</td>
                    <td> <img alt="preview image" width="120" height="120" src={image?image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSjx-by4e5r6kDq59HyeRElGTd00oMFr-duASxN8UKnsQ&s"}/></td>
                    <td>{name}</td>
                    <td>{price}</td>
                    <td>{sellingprice}</td>
                    <td>{stock}</td>
                    <td>
                       <Link to={`/tutorials/${id}`}>
                        <button className="m-3 btn btn-sm btn-success" onClick={this.removeTutorial}>Edit</button>
                      </Link>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
</div>
          {/* <ul className="list-group">
            {tutorials &&
              tutorials.map((tutorial, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveTutorial(tutorial, index)}
                  key={index}
                >
                  {tutorial.name}
                </li>
              ))}
          </ul> */}
  
          <button
            className="m-3 btn btn-sm btn-danger mr-2"
            onClick={this.removeAllTutorials}
          >
            Remove All
          </button>
        </div>
        {/* <div className="col-md-6">
          {currentTutorial ? (
            <div>
              <h4>Tutorial</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>{" "}
                {currentTutorial.title}
              </div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentTutorial.description}
              </div>
              <div>
                <label>
                  <strong>Status:</strong>
                </label>{" "}
                {currentTutorial.published ? "Published" : "Pending"}
              </div>

              <Link
                to={"/tutorials/" + currentTutorial.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>Please click on a Tutorial...</p>
            </div>
          )}
        </div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tutorials: state.tutorials,
  };
};

export default connect(mapStateToProps, {
  retrieveTutorials,
  findTutorialsByTitle,
  deleteAllTutorials,
  deleteTutorial,
})(TutorialsList);
